package com.tesseract.tesseractteam.tesseract.presentation.ui.recorder;

import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenter;

public interface RecorderPresenter extends BasePresenter<RecorderView> {
    void onRecordPressed();

    void onRecordReleased();
}
