package com.tesseract.tesseractteam.tesseract.presentation.ui.main;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenterImpl;


public class MainActivityPresenterImpl extends BasePresenterImpl<MainActivityView> implements MainActivityPresenter {

    public MainActivityPresenterImpl(AppBridge appBridge) {
        super(appBridge);
    }
}
