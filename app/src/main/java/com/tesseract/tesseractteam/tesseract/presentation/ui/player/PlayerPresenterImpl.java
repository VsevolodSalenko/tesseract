package com.tesseract.tesseractteam.tesseract.presentation.ui.player;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenterImpl;

/**
 * Created by seva on 11.11.17.
 */

public class PlayerPresenterImpl extends BasePresenterImpl<PlayerView> implements PlayerPresenter {

    public PlayerPresenterImpl(AppBridge appBridge) {
        super(appBridge);
    }
}
