package com.tesseract.tesseractteam.tesseract.data;

import com.tesseract.tesseractteam.tesseract.data.local.LocalRepository;
import com.tesseract.tesseractteam.tesseract.data.network.NetworkRepository;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

public class TesseractRepositoryImpl implements TesseractRepository {
    private LocalRepository localRepository;
    private NetworkRepository networkRepository;

    public TesseractRepositoryImpl(@NonNull final LocalRepository localRepository,
                                   @NonNull final NetworkRepository networkRepository) {
        this.localRepository = localRepository;
        this.networkRepository = networkRepository;
    }

    public void saveSenderKey(@NonNull final String senderKey) {
        localRepository.saveSenderKey(senderKey);
    }

    @Nullable
    public String getSenderKey() {
        return localRepository.getSenderKey();
    }
}
