package com.tesseract.tesseractteam.tesseract.presentation.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tesseract.tesseractteam.tesseract.R;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BaseActivity;
import com.tesseract.tesseractteam.tesseract.presentation.ui.recorder.RecordActivity;

public class MainActivity extends BaseActivity implements MainActivityView {
    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, RecordActivity.class));
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
}
