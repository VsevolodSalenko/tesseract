package com.tesseract.tesseractteam.tesseract.presentation.ui.recorder;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenterImpl;

public class RecorderPresenterImpl extends BasePresenterImpl<RecorderView> implements RecorderPresenter {
    public RecorderPresenterImpl(AppBridge appBridge) {
        super(appBridge);
    }

    @Override
    public void onRecordPressed() {

    }

    @Override
    public void onRecordReleased() {

    }
}
