package com.tesseract.tesseractteam.tesseract.presentation.core;


import com.tesseract.tesseractteam.tesseract.data.TesseractRepository;

public interface AppBridge {
    TesseractRepository getRepository();
}
