package com.tesseract.tesseractteam.tesseract.presentation.ui.recorder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.AndFlmsg.Modem;
import com.AndFlmsg.Processor;
import com.tesseract.tesseractteam.tesseract.R;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BaseActivity;
import com.tesseract.tesseractteam.tesseract.presentation.ui.player.PlayerActivity;

import butterknife.BindView;

import static com.AndFlmsg.AndFlmsg.bottomToastText;

public class RecordActivity extends BaseActivity implements RecorderView {
    private static final String TAG = "RecordActivity";
    private static final int REQUEST_RECORDER_PERMISSION_CODE = 18;
    @BindView(R.id.recordButton) Button recordButton;
    private RecorderPresenter presenter;
    private MediaRecorder mediaRecorder;
    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private String filePath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORDER_PERMISSION_CODE);
        presenter = new RecorderPresenterImpl(appBridge);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        recordButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        presenter.onRecordPressed();
                        startRecording();
                        Toast.makeText(RecordActivity.this, "press", Toast.LENGTH_SHORT).show();
                        break;
                    case MotionEvent.ACTION_UP:
                        presenter.onRecordReleased();
                        Toast.makeText(RecordActivity.this, "release", Toast.LENGTH_SHORT).show();
                        stopRecording();
                        PlayerActivity.start(RecordActivity.this, filePath);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_record;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORDER_PERMISSION_CODE:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();

    }

    private void startRecording() {
        /*mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        filePath = getAppFolderPath() + "/voice.wav";
        mediaRecorder.setOutputFile(filePath);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "startRecording: prepare() failed", e);
        }

        mediaRecorder.start();*/
        if (!Processor.ReceivingForm) {
            final String intext = "shit";
            Processor.TX_Text += (intext + "\n");
            // Processor.q.send_txrsid_command("ON");
            Modem.txData("", "", intext + "\n", 0, 0, false, "");
        } else {
            bottomToastText("Not while in the middle of receiving a message");
        }
    }

    private void stopRecording() {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
    }

    private String getAppFolderPath() {
        PackageManager m = getPackageManager();
        String s = getPackageName();
        PackageInfo p = null;
        try {
            p = m.getPackageInfo(s, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String appPath = p.applicationInfo.dataDir;
        Log.i(TAG, "getAppFolderPath: " + appPath);
        return appPath;
    }
}
