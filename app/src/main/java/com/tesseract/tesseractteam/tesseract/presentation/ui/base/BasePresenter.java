package com.tesseract.tesseractteam.tesseract.presentation.ui.base;

import io.reactivex.annotations.NonNull;

public interface BasePresenter<View extends BaseView> {

    void bindView(@NonNull final View view);

    void unbindView();

    View getView();
}
