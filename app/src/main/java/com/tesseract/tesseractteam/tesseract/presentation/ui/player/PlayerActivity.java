package com.tesseract.tesseractteam.tesseract.presentation.ui.player;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

import com.tesseract.tesseractteam.tesseract.R;
import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BaseActivity;

import java.io.IOException;

import io.reactivex.annotations.NonNull;

public class PlayerActivity extends BaseActivity implements PlayerView {
    private final static String FILE_PATH_ARG = "filePath";
    private static final String TAG = "PlayerActivity";
    private MediaPlayer mediaPlayer;
    private PlayerPresenter presenter;
    private String filePath;

    public static void start(@NonNull final Context context, @NonNull final String filePath) {
        final Intent starter = new Intent(context, PlayerActivity.class);
        starter.putExtra(FILE_PATH_ARG, filePath);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlayerPresenterImpl(appBridge);
        filePath = getIntent().getStringExtra(FILE_PATH_ARG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        startPlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_player;
    }

    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(filePath);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            Log.e(TAG, "startPlaying: ", e);
        }
    }

    private void stopPlaying() {
        mediaPlayer.release();
        mediaPlayer = null;
    }
}
