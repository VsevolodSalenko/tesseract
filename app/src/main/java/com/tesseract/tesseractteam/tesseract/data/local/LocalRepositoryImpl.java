package com.tesseract.tesseractteam.tesseract.data.local;


import io.reactivex.annotations.NonNull;

public class LocalRepositoryImpl implements LocalRepository {
    private SharedPreferencesManager sharedPreferencesManager;

    public LocalRepositoryImpl(@NonNull final SharedPreferencesManager sharedPreferencesManager) {
        this.sharedPreferencesManager = sharedPreferencesManager;
    }


    @Override
    public void saveSenderKey(@NonNull final String senderKey) {
        sharedPreferencesManager.saveSenderKey(senderKey);
    }

    @Override
    public String getSenderKey() {
        return sharedPreferencesManager.getSenderKey();
    }
}
