package com.tesseract.tesseractteam.tesseract.data.local;

import com.tesseract.tesseractteam.tesseract.data.TesseractRepository;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

public interface LocalRepository extends TesseractRepository {
    void saveSenderKey(@NonNull final String senderKey);

    @Nullable
    String getSenderKey();
}
