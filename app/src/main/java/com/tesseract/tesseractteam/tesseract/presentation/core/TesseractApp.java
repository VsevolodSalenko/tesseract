package com.tesseract.tesseractteam.tesseract.presentation.core;

import android.app.Application;

import com.tesseract.tesseractteam.tesseract.data.TesseractRepository;
import com.tesseract.tesseractteam.tesseract.data.TesseractRepositoryImpl;
import com.tesseract.tesseractteam.tesseract.data.local.LocalRepositoryImpl;
import com.tesseract.tesseractteam.tesseract.data.local.SharedPreferencesManager;
import com.tesseract.tesseractteam.tesseract.data.network.NetworkRepositoryImpl;

public class TesseractApp extends Application implements AppBridge {
    private TesseractRepositoryImpl tesseractRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        tesseractRepository = new TesseractRepositoryImpl(
                new LocalRepositoryImpl(new SharedPreferencesManager(this)),
                new NetworkRepositoryImpl());
    }

    @Override
    public TesseractRepository getRepository() {
        return tesseractRepository;
    }
}
