package com.tesseract.tesseractteam.tesseract.presentation.ui.player;

import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenter;

/**
 * Created by seva on 11.11.17.
 */

public interface PlayerPresenter extends BasePresenter<PlayerView> {
}
