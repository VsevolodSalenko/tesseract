package com.tesseract.tesseractteam.tesseract.presentation.ui.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;

public abstract class BaseFragment extends Fragment implements ButterKnifeView, BaseFragmentView {
    protected AppBridge appBridge;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {
            appBridge = (AppBridge) activity.getApplication();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

}
