package com.tesseract.tesseractteam.tesseract.presentation.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements ButterKnifeView, BaseActivityView {
    protected AppBridge appBridge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        appBridge = (AppBridge) getApplication();
    }
}
