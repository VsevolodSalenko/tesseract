package com.tesseract.tesseractteam.tesseract.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

public class SharedPreferencesManager {
    private static final String SHARED_PREFERENCES_FILE_NAME = "sp";
    private static final String SENDER_KEY = "senderKey";
    private SharedPreferences sharedPreferences;

    public SharedPreferencesManager(@NonNull final Context context) {
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void saveSenderKey(final @NonNull String senderKey) {
        sharedPreferences.edit().putString(SENDER_KEY, senderKey).apply();
    }

    @Nullable
    public String getSenderKey() {
        return sharedPreferences.getString(SENDER_KEY, null);
    }
}
