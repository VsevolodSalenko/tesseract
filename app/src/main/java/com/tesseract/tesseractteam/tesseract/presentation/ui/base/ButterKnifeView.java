package com.tesseract.tesseractteam.tesseract.presentation.ui.base;

import android.support.annotation.LayoutRes;

/**
 * Created by seva on 11.11.17.
 */

public interface ButterKnifeView {
    @LayoutRes
    int getLayoutId();
}
