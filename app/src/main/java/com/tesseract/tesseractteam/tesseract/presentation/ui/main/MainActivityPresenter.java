package com.tesseract.tesseractteam.tesseract.presentation.ui.main;

import com.tesseract.tesseractteam.tesseract.presentation.ui.base.BasePresenter;

public interface MainActivityPresenter extends BasePresenter<MainActivityView> {

}
