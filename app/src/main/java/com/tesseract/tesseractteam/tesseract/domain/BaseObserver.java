package com.tesseract.tesseractteam.tesseract.domain;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;


public class BaseObserver<T> extends DisposableObserver<T> {

    @Override
    public void onNext(@NonNull final T t) {

    }

    @Override
    public void onError(@NonNull final Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}
