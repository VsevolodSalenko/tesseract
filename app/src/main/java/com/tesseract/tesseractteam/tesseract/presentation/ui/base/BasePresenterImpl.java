package com.tesseract.tesseractteam.tesseract.presentation.ui.base;

import com.tesseract.tesseractteam.tesseract.presentation.core.AppBridge;

import java.lang.ref.WeakReference;

import io.reactivex.annotations.NonNull;

public class BasePresenterImpl<View extends BaseView> implements BasePresenter<View> {
    protected AppBridge appBridge;
    private WeakReference<View> view;

    public BasePresenterImpl(@NonNull final AppBridge appBridge) {
        this.appBridge = appBridge;
    }

    public void bindView(@NonNull final View view) {
        this.view = new WeakReference<>(view);
    }

    public void unbindView() {
        view = null;
    }

    public View getView() {
        return view == null ? null : view.get();
    }
}
